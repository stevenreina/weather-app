import { LitElement, html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { createRef, ref } from 'lit/directives/ref.js';
import { when } from 'lit/directives/when.js';

import './components/query-mode';
import './components/query-input';
import './components/query-result';
import { QueryResult } from './components/query-result';
import { baseStyles } from './styles';


export type QueryParams = {
  cityName: string,
  coordinates: Array<string>
};

@customElement('app-root')
export class App extends LitElement {

  logo = new URL('../public/logo.png', import.meta.url);
  queryResultRef = createRef<QueryResult>();

  @state()
  queryModeIsCity = true;
  @state()
  cityName = '';
  @state()
  coordinates = ['', ''];
  @state()
  backgroundColor = '';
  @state()
  hasBeenUsed = false;

  static styles = baseStyles;

  // Evaluates the query parameters, queries the API, and displays the information in a single function
  async _submitQuery () {
    await this.queryResultRef.value?._queryLocationData();
    await this.queryResultRef.value?._queryWeatherData();
  }

  protected render () {
    return html`
      <main>
        <h1><img src=${this.logo} alt="Weather App by Steven Reina"></h1>
        <div id="control-container">
          <query-mode .isCity=${this.queryModeIsCity}></query-mode>
          <query-input
            .queryModeIsCity=${this.queryModeIsCity}
            .coordinates=${this.coordinates}
          ></query-input>
          <button
            id="submit-button"
            @click=${this._submitQuery}
          >Check</button>
        </div>
        ${when(this.hasBeenUsed, () => html`
          <query-result
            .queryModeIsCity=${this.queryModeIsCity}
            .cityNameParam=${this.cityName}
            .coordinates=${this.coordinates}
            ${ref(this.queryResultRef)}
          ></query-result>
          <div id="weather-color" style="box-shadow: 0 0 100px 100px ${this.backgroundColor || 'transparent'}"></div>
        `)}
      </main>
    `;
  }

  connectedCallback () {
    super.connectedCallback();

    this.addEventListener('toggle-query-mode', (e) => {
      this.queryModeIsCity = e.detail;
    });

    this.addEventListener('submit-query', async () => {
      await this.updateComplete;
      await this._submitQuery();
    });

    this.addEventListener('update-query-data', (e) => {
      const { cityName, coordinates } = e.detail;
      this.cityName = cityName;
      this.coordinates = coordinates;
      this.hasBeenUsed = true;
    });

    this.addEventListener('update-background-color', (e) => {
      this.backgroundColor = e.detail;
    });
  }

}

declare global {
  interface HTMLElementTagNameMap {
    'app-root': App
  }
}
