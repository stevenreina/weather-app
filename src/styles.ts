import { css } from "lit";


export const baseStyles = css`
  main {
    color: #FFF;
    background-color: #0E0E0EAA;
    display: flex;
    flex-direction: column;
    align-items: center;
    font: 17px 'Source Sans 3', monospace;
    width: 60vw;
    height: 100svh;
  }

  main > h1 {
    font-size: 2.8rem;
    text-shadow: -4px 4px 0 #AAF8;
    margin-bottom: 1.2rem;
    display: flex;
    justify-content: center;
  }

  main > h1 > img {
    width: 80%;
  }

  #control-container {
    width: 369px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 3.4rem;
  }

  #submit-button {
    color: #999;
    display: block;
    width: 200px;
    margin-top: 1.8rem;
    padding: 1rem;
    font-size: 18px;
    background-color: transparent;
    border: none;
    border-bottom: 2px solid #999;
    cursor: pointer;
  }

  #submit-button:hover {
    border-color: #0077e5;
    color: #0077e5;
  }

  #submit-button:active {
    box-shadow:
      0 0 10px #66C8 inset,
      0 0 4px #3335 inset;
    border-color: #68A8;
    color: #68CC;
  }

  #weather-color {
    min-width: 100vw;
    position: absolute;
    top: 0;
    transition: box-shadow .8s;
    z-index: -1;
  }

  @media (max-width: 530px) {
    #control-container {
      width: 240px;
    }
  }

  @media (max-width: 380px) {
    #control-container {
      width: 200px;
    }
  }
`;

export const queryInputStyles = css`
  #controls {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: .5rem;
    gap: 1rem;
    color: #AAA;

    input {
      box-sizing: border-box;
      width: 100%;
      margin-top: 5px;
      color: #333;
      padding: 5px;
      border: 2px solid #CCC;
      border-radius: 4px;
      display: block;
    }
  }
`;

export const queryModeStyles = css`
  #query-mode {
    color: #CCC;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-bottom: 1.2rem;
    font-size: 1.1rem;
  }

  #query-mode > * {
    width: calc(100%/3);
  }

  #query-mode > div:first-child {
    padding-right: 1rem;
    text-align: right;
  }

  #query-mode > div:last-child {
    padding-left: 1rem;
    text-align: left;
  }

  #query-switch {
    background-color: #DFDFE1;
    display: flex;
    width: 6rem;
    align-items: center;
    height: 2.1rem;
    border: 3px solid #CCC;
    border-radius: 2rem;
    cursor: pointer;
    transition: .2s;
  }

  #query-switch:hover {
    border-color: #999;
  }

  #query-switch > div {
    margin: 0 .2rem;
    display: inline-flex;
    justify-content: right;
    width: 1.8rem;
    transition: .2s;
  }

  #query-switch > div > div {
    background-color: #0077E5;
    height: 1.8rem;
    width: 1.8rem;
    border-radius: 50%;
  }

  #query-mode[city] {
    #query-switch > div {
      width: 1.8rem;
    }
    .query-mode-label:first-child {
      color: #999;
    }
    .query-mode-label:last-child {
      color: revert;
    }
  }

  #query-mode:not([city]) {
    #query-switch > div {
      width: 100%;
    }
    .query-mode-label:first-child {
      color: revert;
    }
    .query-mode-label:last-child {
      color: #999;
    }
  }
`;

export const queryResultStyles = css`
  #query-content {
    padding: 1.1rem;
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    font-family: 'Rubik', sans-serif;
  }

  #weather-content {
    display: flex;
    align-items: center;
    gap: 32px;
    margin-bottom: 1.4rem;
  }

  #city {
    font-size: 2.4rem;
    text-transform: uppercase;
  }

  #city-info > pre {
    font-size: 1.8rem;
    text-transform: uppercase;
    color: #FFF8;
  }

  #forecast {
    font-size: 2.9rem;
  }

  #temperature {
    font-weight: 400;
    font-size: 3.6rem;
  }
`;
