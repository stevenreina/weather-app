import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { queryModeStyles } from '../styles';


@customElement('query-mode')
export class QueryMode extends LitElement {

  @property({ type: Boolean, attribute: false })
  isCity?: boolean;

  static styles = queryModeStyles;

  // Toggles between the query modes
  __toggleQueryMode () {
    this.isCity = !this.isCity;
  }

  protected render () {
    return html`
      <!-- Query mode switch -->
      <div id="query-mode" ?city=${this.isCity}>
        <div class="query-mode-label">City</div>
        <div
          id="query-switch"
          @click=${this.__toggleQueryMode}
        >
          <div>
            <div></div>
          </div>
        </div>
        <div class="query-mode-label">Coordinates</div>
      </div>
    `;
  }

  protected updated () {
    this.dispatchEvent(new CustomEvent<boolean>('toggle-query-mode', {
      bubbles: true,
      composed: true,
      detail: this.isCity
    }));
  }

}

declare global {
  interface HTMLElementTagNameMap {
    'query-mode': QueryMode
  }
  interface HTMLElementEventMap {
    'toggle-query-mode': CustomEvent<boolean>
  }
}
