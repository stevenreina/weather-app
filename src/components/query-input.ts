import { LitElement, html } from 'lit';
import { customElement, property, query, queryAll, state } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';

import { QueryParams } from '../app';
import { checkLocationBy } from '../apiQueries';
import { queryInputStyles } from '../styles';


@customElement('query-input')
export class QueryInput extends LitElement {

  @query('#city-directive')
  cityDirective?: HTMLInputElement
  @queryAll('.directive')
  coordsDirectives?: NodeListOf<HTMLInputElement>

  @property({ type: Boolean, attribute: false })
  queryModeIsCity?: boolean;
  @state()
  cityName = '';
  @property({ type: Array<String>, attribute: false })
  coordinates = ['', ''];

  static styles = queryInputStyles;

  private _updateData () {
    this.dispatchEvent(new CustomEvent<QueryParams>('update-query-data', {
      bubbles: true,
      composed: true,
      detail: {
        cityName: this.cityName,
        coordinates: this.coordinates
      }
    }));
  }

  private _submitQuery () {
    this.dispatchEvent(new CustomEvent('submit-query', {
      bubbles: true,
      composed: true
    }));
  }

  private __submitOnEnter (e: KeyboardEvent) {
    if (e.key == 'Enter')
      this._submitQuery();
  }

  private _setData () {
    if (this.queryModeIsCity) {
      this.cityName = this.cityDirective!.value;
    } else {
      let coords = Array<string>();
      this.coordsDirectives?.forEach((coord) => coords.push(coord.value));
      this.coordinates = coords;
    }
    this._updateData();
  }

  protected render () {
    return html`
       <div id="controls">
         ${when(
           this.queryModeIsCity,
           () => html`
             <label>City name:
               <input type="text"
                 id="city-directive"
                 class="directive"
                 value="${this.cityName}"
                 @keypress=${this.__submitOnEnter}
                 @input=${this._setData}
               >
             </label>
           `,
           () => html`
             <label>Latitude:
               <input type="text"
                 name="latitude"
                 class="directive coordinate-directive"
                 value="${this.coordinates[0]}"
                 @keypress=${this.__submitOnEnter}
                 @input=${this._setData}
               >
             </label>
             <label>Longitude:
               <input type="text"
                 name="longitude"
                 class="directive coordinate-directive"
                 value="${this.coordinates[1]}"
                 @keypress=${this.__submitOnEnter}
                 @input=${this._setData}
               >
             </label>
           `
         )}
       </div>
    `;
  }

  protected firstUpdated () {
    // Ask for location permission
    navigator.geolocation.getCurrentPosition(async (pos) => {
      const coords = pos.coords;
      this.coordinates[0] = coords.latitude.toString();
      this.coordinates[1] = coords.longitude.toString();
      let cityData = await checkLocationBy('coordinates', this.coordinates);
      this.cityName = cityData.display_name.split(', ')[0];

      this._updateData();
      this._submitQuery();
    });
  }

}

declare global {
  interface HTMLElementTagNameMap {
    'query-input': QueryInput
  }
  interface HTMLElementEventMap {
    'update-query-data': CustomEvent<QueryParams>
  }
}
