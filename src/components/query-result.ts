import { LitElement, html } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import { QueryParams } from '../app';
import { checkLocationBy, checkWeather } from '../apiQueries';
import { forecastDecoder, getBgColorsByTemperature } from '../weatherDataHandling';
import { queryResultStyles } from '../styles';


@customElement('query-result')
export class QueryResult extends LitElement {

  @property({ type: Boolean, attribute: false })
  queryModeIsCity?: boolean;
  @property({ type: Array<String>, attribute: false })
  coordinates?: Array<string>;

  @state()
  cityNameParam?: string;
  @state()
  cityName?: string;
  @state()
  cityInfo?: string;
  @state()
  temperature?: number;
  @state()
  forecast?: string;
  @state()
  weatherColors?: Array<string>;

  static styles = queryResultStyles;

  private _updateData () {
    this.dispatchEvent(new CustomEvent<QueryParams>('update-query-data', {
      bubbles: true,
      composed: true,
      detail: {
        cityName: this.cityName!,
        coordinates: this.coordinates!
      }
    }));
  }

  private _updateBackgroundColor (color: string) {
    this.dispatchEvent(new CustomEvent('update-background-color', {
      bubbles: true,
      composed: true,
      detail: color
    }));
  }

  // Checks, arranges and displays location data
  async _queryLocationData () {
    let locationData;
    if (this.queryModeIsCity) {
      locationData = await checkLocationBy('cityName', [this.cityNameParam!]);
      locationData = locationData[0];
    } else {
      locationData = await checkLocationBy('coordinates', this.coordinates);
    }
    this.coordinates = [locationData.lat, locationData.lon];
    this._updateData();

    let cityInfo = locationData.display_name.split(', ');
    let city = cityInfo.shift();
    cityInfo = cityInfo.join('\n');

    this.cityName = city;
    this.cityInfo = cityInfo;
  }

  // Checks and displays weather data
  async _queryWeatherData () {
    const { temperature, weathercode } = await checkWeather(this.coordinates);
    this.forecast = forecastDecoder(weathercode);
    this.temperature = temperature;

    const { r, g, b } = getBgColorsByTemperature(temperature);
    this._updateBackgroundColor(`rgb(${r}, ${g}, ${b})`);
  }

  protected render () {
    return html`
      <!-- Query content as a result -->
      <div id="query-content">
        <div id="weather-content">
          <div id="temperature">${this.temperature ? `${this.temperature}ºC` : ''}</div>
          <div id="forecast">${this.forecast}</div>
        </div>
        <div id="city">${this.cityName || 'Unknown'}</div>
        <div id="city-info"><pre>${this.cityInfo || 'Unknown'}</pre></div>
      </div>
    `;
  }

}

declare global {
  interface HTMLElementTagNameMap {
    'query-result': QueryResult
  }
  interface HTMLElementEventMap {
    'update-background-color': CustomEvent<string>
  }
}
