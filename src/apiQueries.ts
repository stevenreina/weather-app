
type WeatherDataChunk = {
  temperature: number;
  weathercode: number;
};


// Checks location with custom query gotten by geolocation API
// (Nominatim Geocoding API)
export async function checkLocationBy <QueryType = 'cityName' | 'coordinates'>
    (queryType?: QueryType, query?: Array<string>) {
  const baseUrl = 'https://nominatim.openstreetmap.org';
  const endpointQuery = queryType == 'coordinates'
      ? `/reverse?format=json&lat=${query![0]}&lon=${query![1]}&zoom=10`
      : `/search?format=json&city=${query![0]}`;
  try {
    let locationData = await fetch(baseUrl + endpointQuery)
        .then((data) => data.json());
    return locationData;
  } catch (e) {
    console.error(e);
    return {};
  }
}

// Checks weather with weather API
// (Open-Meteo Weather API)
export async function checkWeather (coordinates?: Array<string>) {
  const [latitude, longitude] = coordinates!;
  const baseUrl = 'https://api.open-meteo.com';
  const endpointQuery = `/v1/forecast?latitude=${latitude}&longitude=${longitude}&current_weather=true`;
  const weatherData = await fetch(baseUrl + endpointQuery)
      .then((data) => data.json()) satisfies { current_weather: WeatherDataChunk };
  if (weatherData.current_weather == undefined) {
    console.error('Wasn\'t possible to retrieve the weather data!');
    return { temperature: NaN, weathercode: -1 };
  }
  return weatherData.current_weather;
}
